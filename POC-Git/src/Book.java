public class Book {

    /* attribute */
    Integer width;
    Integer height;
    String colorOfCoverPage;
    String name;


    /* method */
    public void getName(){
        System.out.println("name:"+ this.name);
    }

    public void getWidth(){
        System.out.println("width:"+this.width);
    }

    public void getHeight(){
        System.out.println("height:" + this.height);
    }

    public void getColorOfCoverPage(){
        System.out.println("color:" + this.colorOfCoverPage);
    }
}
