public class Pen {

    /* attribute*/
    String color;
    Integer price;
    Integer width;

    /* method */

    public void canWrite(){
        System.out.println("Write");
    }

    public void canKill(){
        System.out.println("Killed by Pen");
    }

    public void canSell(){
        System.out.println("Sell");
    }
}
